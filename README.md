###Project for developer document of Elefos.ATP

This is the sourcecode of ATP docs at: http://atp-docs.elefos.io/en/latest/

####Introduction
https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html
Powered by readthedocs.io
Built by mkdocs


#### How to deploy
Login to https://readthedocs.org/dashboard/ with username `thanh.parth` and deploy after commiting the code.
