# Welcome to Elefos.ATP's documentation!

This document helps you to understand how to integrate Elefos ATP with your system.

## Quick start

- First, login to the [portal of Elefos ATP](https://atp-portal.elefos.io/auth/login)

- Get your `Organization Id` at: `Organizations` > `Organizations`, get the id in column `HashedId`

- Go to `Organizations` > `Organization Elefos Account` to input your account private key (If you choose the method to keep private key on Elefos ATP system).

- Let's create your first Api Key, from the menu: Go to `Access` > `Api Keys` > Click `+` button, select your organization and click "Add" to add new Api key. Then click to the new Api Key created, you can copy your `Key`.

#### Create your first token
Go to `Access` > `Internal Token`. You can see all of the token under your accounts. The `Token Name` is the name of the token in your API calls. Any token which has `Supply / Max Supply` equal to "0 / 0" is the not created yet, let's call the API to create the token on Elefos blockchain
```
POST https://atp-api.elefos.io/v1/token/tx/create
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&token_description={Description for the Token}
	&max_supply={Maximum supply unit for this token}
```
Example:
```
curl -X POST \
   'https://atp-api.elefos.io/v1/token/tx/create?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X'
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&token_description=Issue%20coin5%20for%20testing&max_supply=1000000000000000000'
```

#### Issue token units for your account
```
POST: https://atp-api.elefos.io/v1/token/tx/issue
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&quantity={Unit to issue}
	&custom_memo={Custom memo}
	&meta_id={Meta ID string for lookup from partner side}
```
Example:
```
curl -X POST \
    'https://atp-api.elefos.io/v1/token/tx/issue?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&quantity=100&custom_memo=test&meta_id=abc123'
```

#### Check balance of internal account
When you issue your token, it go to your "Base" internal account which has ID 0. Let's check your account balance using the check balance API:
```
GET https://atp-api.elefos.io/v1/token/access/balance
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
	&id={Internal ID}
	&token_name={Token Name}
```
Example:
```
curl -X GET \
    'https://atp-api.elefos.io/v1/token/access/balance?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X&id=0&token_name=coin1'
```

#### Transfer your token between internal accounts
Internal accounts are numbered from 0, the ID 0 is for the base account, other ID > 0 can be treated as an individual user in your system
```
POST https://atp-api.elefos.io/v1/token/tx/transfer
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&from_id={Internal ID}
	&to_id={Internal ID}
	&quantity={Quantity}
	&meta_id={Meta ID string for lookup from partner side}
```
Example:
```
curl -X POST \
    'https://atp-api.elefos.io/v1/token/tx/transfer?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&from_id=1&to_id=4&quantity=1&meta_id=xyz123'
```

#### Get log of your tx from your side
You can check the status of tx when calling from your side, when you call API for internal transfer / issue token, there will be a `logId` response which is used to check the status of your tx. You can also use the `meta_id` which is an reference id from your side to check the status in case there is any disconnection between two sides.
```
GET https://atp-api.elefos.io/v1/check/tx_log
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
	&id={ID of your tx, which is the logId value}
	&meta_id={Or you can use a meta_id to check}
```
Example:
```
curl -X GET \
  'https://atp-api.elefos.io/v1/check/tx_log?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X&meta_id=xyz123' 
```

## Support

If you are having issues, please let us know.
We have a mailing list located at: `thanh@parthlabs.io`

## License

The project is licensed under Elefos.io
