API Reference Page for Elefos ATP system.

## Get account balance (Internal)
```
GET https://atp-api.elefos.io/v1/token/access/balance
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
	&id={Internal ID}
	&token_name={Token Name}
```
Example:
```
curl -X GET \
    'https://atp-api.elefos.io/v1/token/access/balance?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X&id=0&token_name=coin1'
```
Response:
```
{
    "balance": 999998050,
    "internalId": 0,
    "tokenName": "coin1"
}
```

## Get Token information
```
GET https://atp-api.elefos.io/v1/token/access/info
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
	&token_name={Token Name}
```
Example:
```
curl -X GET \
    'https://atp-api.elefos.io/v1/token/access/info?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X&token_name=coin1'
```
Response:
```
{
    "tokenName": "coin1",
    "tokenDescription": "Loyalty token for CGV",
    "issuer": "thanh",
    "supply": 1000000100,
    "maxSupply": 1000000000000000000,
    "internalSupply": 999999050
}
```

## Create Token

```
POST https://atp-api.elefos.io/v1/token/tx/create
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&token_description={Description for the Token}
	&max_supply={Maximum supply unit for this token}
```
Example:
```
curl -X POST \
   'https://atp-api.elefos.io/v1/token/tx/create?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X'
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&token_description=Issue%20coin5%20for%20testing&max_supply=1000000000000000000'
```

## Issue Token
Issued token are added to your internal base account (ID 0)
```
POST: https://atp-api.elefos.io/v1/token/tx/issue
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&quantity={Unit to issue}
	&custom_memo={Custom memo}
	&meta_id={Meta ID string for lookup from partner side}
```
Example:
```
curl -X POST \
    'https://atp-api.elefos.io/v1/token/tx/issue?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&quantity=100&custom_memo=test&meta_id=abc123'
```

## Transfer Token (Internal)
Internal accounts are numbered from 0, the ID 0 is for the base account, other ID > 0 can be treated as an individual user in your system
```
POST https://atp-api.elefos.io/v1/token/tx/transfer
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: token_name={Token Name}
	&from_id={Internal ID}
	&to_id={Internal ID}
	&quantity={Quantity}
	&meta_id={Meta ID string for lookup from partner side}
```
Example:
```
curl -X POST \
    'https://atp-api.elefos.io/v1/token/tx/transfer?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'token_name=coin1&from_id=1&to_id=4&quantity=1&meta_id=xyz123'
```

## Update Internal user information

```
POST https://atp-api.elefos.io/v1/internal_user/update
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
BODY: internal_id={Intertal ID}
	&name={Name of account}
        &meta={More meta information}
	&brand_id={Brand Id of this account}
	&chain_id={Chain Id of this account}
	&store_id={Store Id of this account}
```
Example:
```
curl -X POST \
    'https://atp-api.elefos.io/v1/internal_user/update?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reT7Kivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X' \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d 'internal_id=3&name=User%20%233&meta=12425&brand_id=1&chain_id=1&store_id=1'
```

## Check tx status using meta_id or logId
```
GET https://atp-api.elefos.io/v1/check/tx_log
	?organization_id={YOUR ORGANIZATION ID}
	&api_key={YOUR API KEY}
	&id={You can use the logId value in response of a API call}
	&meta_id={Or you can use a meta_id generated from your side to check}
```
Example:
```
curl -X GET \
  'https://atp-api.elefos.io/v1/check/tx_log?organization_id=Gp6Jg4nvEax2P0OU8VK9RkALNqwzW7&api_key=d9d4fr.reQvKivr1pEQkyvxBAye1jUiwEQzSKNw5KT3i1LpF2WZwv9oFe4FFUQ4G90X&meta_id=xyz123' 
```
Note: In case you attach a meta_id that is used before from your system, the value will be assigned to the new Tx.


